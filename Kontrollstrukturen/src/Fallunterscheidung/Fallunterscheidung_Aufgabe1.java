package Fallunterscheidung;
import java.util.Scanner;

public class Fallunterscheidung_Aufgabe1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben sie eine Note: \n");
		int op = scanner.nextInt();
		
		switch(op) {
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("gut");
			break;
		case 3:
			System.out.println("befriedigend");
			break;
		case 4:
			System.out.println("ausreichend");
			break;
		case 5:
			System.out.println("mangelhaft");
			break;
		case 6:
			System.out.println("ungenügend");
			break;
		default:
			System.out.println("Fehler");
			
		}

	}

}
