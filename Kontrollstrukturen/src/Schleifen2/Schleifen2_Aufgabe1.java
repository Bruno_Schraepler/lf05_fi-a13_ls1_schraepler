package Schleifen2;
import java.util.Scanner;

public class Schleifen2_Aufgabe1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie die Grenze n: ");
		int n = scanner.nextInt();
		
		//Z�hlvariable definieren
		int acc = 1;
		
		//Zahlen von 1 bis n ausgeben
		while(acc<=n) {
			System.out.println(acc);
			acc++;
		}
		
		acc = n;
		
		//Z�hlen von n bis 1
		while(acc > 0) {
			System.out.println(acc);
			acc--;
		}

	}

}
