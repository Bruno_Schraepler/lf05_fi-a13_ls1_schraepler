package AB_Auswahlstrukturen;
import java.util.Scanner;


public class Auswahlstrukturen_Aufgabe1_1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("1. Zahl: ");
		int zahl1 = scanner.nextInt();
		
		System.out.println("2. Zahl: ");
		int zahl2 = scanner.nextInt();
		
		System.out.println("3. Zahl: ");
		int zahl3 = scanner.nextInt();
		
		//Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden 
		if((zahl1 > zahl2) && (zahl1 > zahl3)) {
			System.out.println("Die 1.Zahl ist gr��er als die 2. und 3. Zahl");
		}
		
		//Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung ausgegeben werden
		if((zahl1 > zahl2) || (zahl3 > zahl2)) {
			System.out.println("Die 3. Zahl ist gr��er als die 2. oder die 1. Zahl");
		}
		
		//Geben Sie die gr��te der 3 Zahlen aus
		if((zahl1 > zahl2) && (zahl1 > zahl3)) {
			System.out.println("Die gr��te Zahl: "+zahl1);
		} else if((zahl2 > zahl1) && (zahl2 > zahl3)) {
			System.out.println("Die gr��te Zahl: "+zahl2);
		} else if((zahl3 > zahl1) && (zahl3 > zahl2)) {
			System.out.println("Die gr��te Zahl: "+zahl3);
		} else {
			System.out.println("Fehler");
		}
		

	}

}
