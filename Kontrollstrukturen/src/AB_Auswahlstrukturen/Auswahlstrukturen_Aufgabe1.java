package AB_Auswahlstrukturen;
import java.util.Scanner;

public class Auswahlstrukturen_Aufgabe1 {

	public static void main(String[] args) {
		// Eingabe zweier Zahlen durch den User
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie die erste Zahl: ");
		int zahl1 = scanner.nextInt();
		
		System.out.println("Geben Sie die zweite Zahl: ");
		int zahl2 = scanner.nextInt();
		
		//Wenn beide Zahlen gleich gro� sind soll eine Meldung ausgegeben werden
		if(zahl1 == zahl2) {
			System.out.println("Beide Zahlen sind gleich gro�");
		}
		
		//Wenn die zweite Zahl gr��er als die erste Zahl ist, soll eine Meldung ausgegeben werden
		if(zahl2 > zahl1) {
			System.out.println("Die zweite Zahl ist gr��er als die erste");
		}
		
		//Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben werden, ansonsten eine andere Meldung (If-Else)
		if(zahl1 >= zahl2) {
			System.out.println("Die 1. Zahl ist gr��re oder gleich der zweiten Zahl");
		} else {
			System.out.println("die 2. Zahl ist gr��er als die 1. Zahl");
		}

	}

}
