package Schleifen1;
import java.util.Scanner;

public class Schleifen1_Aufgabe1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		//Eingabe von n
		System.out.println("Geben sie die Grenze n: ");
		int n = scanner.nextInt();
		
		// a) Ausgabe aller natürlichen Zahlen von 1 bis n
		for(int i = 1; i <= n; i++) {
			System.out.println(i);
		}
		
		// b) Ausgabe aller natürlichen Zahlen von n bis 1
		for(int i = n; i > 0; i--) {
			System.out.println(i);
		}
		
	}
}
