package Schleifen1;
import java.util.Scanner;

public class Schleifen1_Aufgabe2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Legen Sie n fest: ");
		int n = scanner.nextInt();
		
		// a)
		int acc = 0;
		for(int i = 1; i <= n; i++) {
			acc = acc+i;
		}
		
		System.out.println("Summe der Zahlen von 1 bis n = "+acc);
		
		// b)
		acc = 0;
		for(int i = 2; i <= (2*n); i += 2) {
			acc = acc+i;
		}
		
		System.out.println("Die Summe der Zahlen von 2 bis 2n (in 2er Schritten) = "+ acc);
		
		// c)
		acc = 0;
		for (int i = 1; i <= ((2*n) +1); i += 2) {
			acc = acc+i;
		}
		
		System.out.println("Die Summe der Zahlen von 1 bis 2n+1 (in 2er Schritten = "+ acc);
	

	}

}
