package Schleifen1;

public class Schleifen1_Aufgabe3 {

	public static void main(String[] args) {
		
		//Test auf Teilbarkeit durch 7 f�r Zahlen 1-200
		for (int i = 0; i<= 200; i++) {
			if((i%7) == 0) {
				System.out.println(i);
			}
		}
		
		System.out.println("\nPr�fung auf Teilbarkeit durch 4, nicht aber durch 5\n");
		//test auf Teilbarkeit durch 4, nicht aber durch 5
		for(int i = 0; i<= 200; i++) {
			if((i%4) == 0) {
				if((i%5) != 0) {
					System.out.println(i);
				}
			}
		}

	}

}
