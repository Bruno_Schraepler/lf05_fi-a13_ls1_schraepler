package AB_EinfacheUebungenZuArrays;

public class Aufgabe1 {

	public static void main(String[] args) {
		//Array Zahlen erschaffen
		int[] zahlen = new int[10];
		
		//Array bef�llen mit Zahlen von 0-9
		for(int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		//Ausgabe mit for-each-loop
		System.out.print("[");
		for(int i : zahlen) {
			System.out.printf(" %d,", i);
		}
		System.out.print("]");
	
	}

}
