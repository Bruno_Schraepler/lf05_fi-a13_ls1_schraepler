package AB_EinfacheUebungenZuArrays;

import java.util.Arrays;

public class Aufgabe4_alternative {

	public static void main(String[] args) {
		//Lotto
		
		//Variablen  erschaffen
		int[] lotto = {3,7,12,18,37,42};
		boolean num12Vorhanden;
		boolean num13Vorhanden;
		
		//Aufgabe a : Schleifenbasierte Ausgabe
		System.out.print("[");
		for(int i : lotto) {
			System.out.printf(" %d ", i);
		}
		System.out.print("]");
		
		//Aufgabe b : Pr�fung ob Zahlen 12 und 13 im Array vorhanden sind
		
		//num12Vorhanden = Arrays.stream(lotto).anyMatch(i -> i == 12);
		num12Vorhanden = Arrays.asList(lotto).contains(12);
		//num13Vorhanden = Arrays.stream(lotto).anyMatch(i -> i == 13);
		num13Vorhanden = Arrays.asList(lotto).contains(13);
		
		
		//finale Ausgabe
		String ausgabef = "Die Zahl %d %s in der Ausgabe vorhanden\n";
		
		System.out.println("\n"); //formatierung
		
		if(num12Vorhanden) {
			System.out.printf(ausgabef, 12, "ist");
		} else {
			System.out.printf(ausgabef, 12, "ist nicht");
		}
		
		if(num13Vorhanden) {
			System.out.printf(ausgabef, 13, "ist");
		} else {
			System.out.printf(ausgabef, 13, "ist nicht");
		}
	}

}
