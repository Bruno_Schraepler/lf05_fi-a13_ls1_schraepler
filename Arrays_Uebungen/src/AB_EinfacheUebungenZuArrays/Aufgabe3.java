package AB_EinfacheUebungenZuArrays;

import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		//Palindrom
		
		//Array und Scanner erschaffen
		char[] charArray = new char[5];						//unsch�n weil Zeicheneingabe nicht variabel lang - Eingabe vor Arraydeklaration erfassen
		Scanner scanner = new Scanner(System.in);			//													und Arraygr��e auf L�nge des Strings setzen
		
		System.out.println("Geben sie nun 5 beliebige Tastaturzeichen ein (Leerzeichen z�hlen ebenfalls): ");
		String eingabe = scanner.next();
		
		//einzelne Zeichen in Array
		for(int i = 0; i < eingabe.length(); i++) {
			charArray[i] = eingabe.charAt(i);
		}
		
		//Ausgabe
		System.out.print("[");
		for(char a : charArray) {
			System.out.printf(" %s,", a);
		}
		System.out.print("]");
		
		scanner.close();

	}

}
