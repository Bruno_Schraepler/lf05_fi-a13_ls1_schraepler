package AB_EinfacheUebungenZuArrays;

public class Aufgabe2 {

	public static void main(String[] args) {
		//ungerade Zahlen
		
		//Array erschaffen
		int[] zahlen = new int[10];
		
		//Bef�llen mit ungeraden Zahlen von 1-19
		int acc = 0;									//acc beschreibt die Indices im Array
		for(int i = 0; acc < zahlen.length; i++) {		//i beschreibt die einzutragenden Zahlen
			if(i%2 != 0) {
				zahlen[acc] = i;
				acc++;
			}
		}
		
		//Ausgabe mit for-each-loop
		System.out.print("[");
		for(int i : zahlen) {
			System.out.printf(" %d,", i);
		}
		System.out.print("]");
	}

}
