package AB_EinfacheUebungenZuArrays;

public class Aufgabe4 {

	public static void main(String[] args) {
		//Lotto
		
		//Array erschaffen
		int[] lotto = {3,7,12,18,37,42};
		
		//Aufgabe a : Schleifenbasierte Ausgabe
		System.out.print("[");
		for(int i : lotto) {
			System.out.printf(" %d ", i);
		}
		System.out.print("]");
		
		//Aufgabe b : Pr�fung ob Zahlen 12 und 13 im Array vorhanden sind
		boolean num12_vorhanden = false;
		for(int i : lotto) {
			if(i == 12) {
				num12_vorhanden = true;
			}
		}
		
		boolean num13_vorhanden = false;
		for(int i : lotto) {
			if(i == 13) {
				num13_vorhanden = true;
			}
		}
		
		
		//finale Ausgabe
		String ausgabef = "Die Zahl %d %s in der Ausgabe vorhanden\n";
		
		System.out.println("\n"); //formatierung
		
		if(num12_vorhanden) {
			System.out.printf(ausgabef, 12, "ist");
		} else {
			System.out.printf(ausgabef, 12, "ist nicht");
		}
		
		if(num13_vorhanden) {
			System.out.printf(ausgabef, 13, "ist");
		} else {
			System.out.printf(ausgabef, 13, "ist nicht");
		}
	}

}
