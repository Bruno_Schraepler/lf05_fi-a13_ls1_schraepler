﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	//Notiz: Ich habe die Variable rückgabebetrag in rückgabebetrag_inCent umgeändert und in ihr den Rückgabebetrag in ct statt in Euro gespeichert, um Ungenauigkeiten
	//bei der Berechnung mit Gleitkommazahlen zu vermeiden.
	
    public static void main(String[] args)
    {
    	//Ticket Array
    	final String[] TICKETS = {
    			"Einzelfahrschein Berlin AB",
    			"Einzelfahrschein Berlin BC",
    			"Einzelfahrschein Berlin ABC",
    			"Kurzstrecke",
    			"Tageskarte Berlin AB",
    			"Tageskarte Berlin BC",
    			"Tageskarte Berlin ABC",
    			"Kleingruppen-Tageskarte Berlin AB",
    			"Kleingruppen-Tageskarte Berlin BC",
    			"Kleingruppen-Tageslarte Berlin ABC"
    	};
    	//Preis Arrays
    	final double[] PREISE = {
    			2.90,
    			3.30,
    			3.60,
    			1.90,
    			8.60,
    			9.00,
    			9.60,
    			23.50,
    			24.30,
    			24.90
    	};
    	
    	//weitere Variablen
    	double zuZahlenderBetrag;
    	double rückgabebetrag_inCent;
    	
    	//Programm
    	while(true) {
    		//Kundenwünsche abfragen / Preis errechnen
    		//----------------------
    		zuZahlenderBetrag = fahrkartenbestellungErfassen(TICKETS, PREISE);
           
    		// Geldeinwurf
    		// -----------
    		rückgabebetrag_inCent = fahrkartenBezahlen(zuZahlenderBetrag); 		
           
    		//Fahrkarten ausgeben
    		//-------------------
    		fahrkartenAusgeben();

    		// Rückgeldberechnung und -Ausgabe
    		// -------------------------------																			
    		rueckgeldAusgeben(rückgabebetrag_inCent);
           
    		//Abschluss
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            	"vor Fahrtantritt entwerten zu lassen!\n"+
                            	"Wir wünschen Ihnen eine gute Fahrt.\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen(final String[] tickets, final double[] preise) {
    	//erfasst die Wünsche des Kunden
    	
    	Scanner scanner = new Scanner(System.in);									//Objekt der Klasse Scanner erschaffen
    	
    	byte auswahl;
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:\n");
    	for(int i = 0; i < tickets.length; i++) {
    		System.out.printf("(%d) %s [%.2f]\n", i+1, tickets[i], preise[i]);
    	}
    	
    	System.out.println();					//Formatierung
    	
    	boolean done = false;
    	do {
    		System.out.print("Ihre Wahl>> ");
    		auswahl = scanner.nextByte();
    		if(auswahl <= tickets.length) {
    			done = true;
    		} else {
    			System.out.println("\n>>ungültige Eingabe<<");
    			done = false;
    		}
    	} while(!done);
    	
    	//einzelpreis
    	double einzelpreis = preise[auswahl-1];			//Subtraktion mit 1, da die Ticketnummern bei 1, die Indizes allerdings bei 0 beginnen
    	
    	System.out.print("Anzahl Tickets>> ");
    	byte anzahlFahrkarten = scanner.nextByte();
    	System.out.println();
    	
    	return einzelpreis*anzahlFahrkarten;
        
    }    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	//übernimmt den Münzeinwurf und berechnet den Rückgabebetrag
    	
    	Scanner scanner = new Scanner(System.in);									//Objekt der Klasse Scanner erschaffen
    	
    	//lokale Variablen definieren die zur Berechnung notwendig sind
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	//Berechnung
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro)>> ");
     	   eingeworfeneMünze = scanner.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        
    	return (eingezahlterGesamtbetrag*100) - (zuZahlenderBetrag*100);	//Berechnung des Rückgabebetrags mit Ganzzahlen (in Cent), um Ungenauigkeiten 
    																		//bei der Rechnung mit Gleitkommazahlen zu vermeiden
    }
    
    
    public static void fahrkartenAusgeben(){
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 10; i++)
        {
           System.out.print("=");
           warte(250); 					
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag_inCent) {
    	
    	if(rückgabebetrag_inCent > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", (rückgabebetrag_inCent/100)); //Umrechnung ct in Euro
     	   System.out.println("wird in folgenden Münzen ausgezahlt:\n");

            while(rückgabebetrag_inCent >= 200) // 2 EURO-Münzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          rückgabebetrag_inCent -= 200;
            }
            while(rückgabebetrag_inCent >= 100) // 1 EURO-Münzen
            {
         	  muenzeAusgeben(1, "EURO");
 	          rückgabebetrag_inCent -= 100;
            }
            while(rückgabebetrag_inCent >= 50) // 50 CENT-Münzen
            {
         	  muenzeAusgeben(50, "CENT");
 	          rückgabebetrag_inCent -= 50;
            }
            while(rückgabebetrag_inCent >= 20) // 20 CENT-Münzen
            {
         	  muenzeAusgeben(20, "CENT");
  	          rückgabebetrag_inCent -= 20;
            }
            while(rückgabebetrag_inCent >= 10) // 10 CENT-Münzen
            {
         	  muenzeAusgeben(10, "CENT");
 	          rückgabebetrag_inCent -= 10;
            }
            while(rückgabebetrag_inCent >= 5)// 5 CENT-Münzen
            {
         	  muenzeAusgeben(5, "CENT");
  	          rückgabebetrag_inCent -= 5;
            }
        }
    }
    
    //Bearbeitung Aufgabe 3.4 - Fahrkartenautomat um weitere Methoden erweitern
    
    public static void warte(int millisekunde) {
    	//unterbricht den aktuellen Thread für die übergebene Zeit in millisekunden
    	try {
    		Thread.sleep(millisekunde);
    	} catch (InterruptedException e) {
    		e.printStackTrace();
    	}
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	//gibt den Übergebenen Betrag mit der übergebenen Einheit (CENT oder EURO) in der Konsole aus
    	System.out.printf("%d %s \n", betrag, einheit);
    }
}