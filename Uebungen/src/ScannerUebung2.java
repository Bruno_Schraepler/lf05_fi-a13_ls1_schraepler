import java.util.Scanner;

public class ScannerUebung2 {
	
	//Aufgabe: Erstellen Sie ein neues Programm. Begr��en Sie den Nutzer und fragen ihn nach seinem
	//Namen und seinem Alter. Speichern Sie beide Eingaben in Variablen und geben diese am
	//Ende des Programms in der Konsole aus.
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); //Objekt der Scanner-class initialisieren
		
		System.out.println("Guten Tag!\nWie hei�en Sie? ");
		String name = scanner.next(); //eingebene Zeile auslesen
		
		System.out.println("Guten Tag "+name);
		System.out.println("Wie alt sind Sie "+name);
		
		int alter = scanner.nextInt(); //eingegebene Ganzzahl auslesen
		
		System.out.println("Es freut mich zu h�ren, dass Sie "+alter+" Jahre alt sind.");
		
		scanner.close();
		
		
	}

}
