package MethodenUebungen;

public class Mathe {
	
	public static void main(String[] args) {
		double ergebnis = Mathe.hypothenuse(12, 15);
		System.out.println("Die Hypothenus der Kathehen betr�gt: "+ergebnis);
	}
	
	public static double quadrat(double x) {
		return Math.pow(x,2.0); //Zugriff auf die in Java Vorinstallierte Math-Klasse
	}
	
	public static double hypothenuse(double kathete1, double kathete2) {
		return Math.sqrt(quadrat(kathete1)+quadrat(kathete2));
	}

}
