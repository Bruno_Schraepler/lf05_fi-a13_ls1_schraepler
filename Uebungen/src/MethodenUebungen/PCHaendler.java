package MethodenUebungen;

import java.util.Scanner;

public class PCHaendler {
	
	public static void main(String[] args) {
		
		//Eingeben
		String artikel = liesString("was m�chten Sie bestellen");
		int anzahl = liesInt("Geben Sie die Anzahl ein");
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		double nettoGesamtPreis = berechneGesamtNettoPreis(anzahl, preis);
		double bruttoGesamtPreis = berechneGesamtBruttoPreis(nettoGesamtPreis, mwst);

		// Ausgeben
		rechnungAusgeben(artikel, anzahl, nettoGesamtPreis, bruttoGesamtPreis, mwst);
		

	}
	
	public static String liesString(String text) {
		//Methode liest einen String aus der Konsole aus
		Scanner scannerString = new Scanner(System.in);
		System.out.println(text);
		String ausgabe = scannerString.next();
		return ausgabe;
	}
	
	public static int liesInt(String text) {
		//Methode liest einen Integer-Wert aus der Konsole aus
		Scanner scannerInt = new Scanner(System.in);
		System.out.println(text);
		int ausgabe = scannerInt.nextInt();
		return ausgabe;
	}
	
	
	public static double liesDouble(String text) {
		//Methode liest einen Double-Wert aus der Konsole aus
		Scanner scannerDouble = new Scanner(System.in);
		System.out.println(text);
		double ausgabe = scannerDouble.nextDouble();
		return ausgabe;
		
	}
	
	public static double berechneGesamtNettoPreis(int anzahl, double nettopreis) {
		//Methode berechnet Gesamt-Netto-Preis
		return anzahl*nettopreis;
	}
	
	public static double berechneGesamtBruttoPreis(double nettoGesamtPreis, double mwst) {
		//Methode berechnet Gesamt-Brutto-Preis
		return nettoGesamtPreis+(nettoGesamtPreis*(mwst/100.0));
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettoGesamtPreis, double bruttoGesamtPreis, double mwst) {
		//Methode gibt die fertige Rechnung aus
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtPreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtPreis, mwst, "%");
	}
	
}

