package MethodenUebungen;
import java.util.Scanner;

public class Fahrsimulator {
	
	public static void main(String[] args) {
		double v = 0.0;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Derzeitige Geschwindigkeit: "+v+"\nGeben Sie eine Geschwindigkeitsveränderung dv in km/h an: ");
		double dv = scanner.nextDouble();
		
		v = beschleunige(v,dv);
		System.out.println("Die neue Geschwindigkeit beträgt: "+v+" km/h");
		
		scanner.close();

	}
	
	public static double beschleunige(double v, double dv) {
		double v_new = v+dv;
		if(v_new<0.0) {
			v = 0.0;
			System.out.println("V_MIN = 0 km/h");
		} else if (v_new>130.0) {
			v = 130.0;
			System.out.println("V_MAX = 130 km/h");
		} else {
			v = v_new;
		}
		
		return v;
	}

}
