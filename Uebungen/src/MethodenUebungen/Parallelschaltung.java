package MethodenUebungen;

public class Parallelschaltung {

	public static void main(String[] args) {
		double r1 = 20.0;
		double r2 = 30.0;
		double ersatzwiderstand = parallelschaltung(r1,r2);
		System.out.println(ersatzwiderstand);

	}
	
	public static double parallelschaltung(double r1, double r2) {
		return (r1*r2)/(r1+r2);
	}

}
