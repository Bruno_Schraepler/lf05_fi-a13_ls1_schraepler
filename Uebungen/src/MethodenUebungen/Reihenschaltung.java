package MethodenUebungen;

public class Reihenschaltung {

	public static void main(String[] args) {
		double r1 = 20.0;
		double r2 = 30.0;
		double ersatzWiderstand = reihenschaltung(r1,r2);
		System.out.println(ersatzWiderstand);
	}
	
	public static double reihenschaltung(double r1, double r2) {
		return r1+r2;
	}

}
