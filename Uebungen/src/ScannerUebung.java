import java.util.Scanner;

public class ScannerUebung {
	
//Aufgabe: �ndern Sie den vorgegebenen Quellcode so ab, dass mit den gegebenen Zahlen alle grundlegenden Rechenoperatoren durchgef�hrt werden

	public static void main(String[] args) {
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnis zugewiesen.
		 System.out.println(zahl1+" + "+zahl2+" = "+(zahl1+zahl2));
		 System.out.println(zahl1+" - "+zahl2+" = "+(zahl1-zahl2));
		 System.out.println(zahl1+" * "+zahl2+" = "+(zahl1*zahl2));
		 System.out.println(zahl1+" / "+zahl2+" = "+(zahl1/zahl2)+" Rest: "+(zahl1%zahl2));
		 myScanner.close();


	}

}
