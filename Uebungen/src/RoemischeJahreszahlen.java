import java.util.Scanner;

public class RoemischeJahreszahlen {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie ein römische Jahreszahl in Großbuchstaben ein: ");
		String jahreszahl = scanner.next();
		
		int lenght = jahreszahl.length();
		int sum = 0;
		
		System.out.println(umwandeln(jahreszahl, lenght, sum));
		
		scanner.close();
		}
	
	public static int romeToDez(char character) {
		switch(character) {
		case 'I':
			return 1;
		case 'V':
			return 5;
		case 'X':
			return 10;
		case 'L':
			return 50;
		case 'C':
			return 100;
		case 'D':
			return 500;
		case 'M':
			return 1000;
		default:
			System.out.print("Invalid Character");
			return 0;
		}
	}
	
	public static int umwandeln(String roem, int lenght, int sum) {
		for(int i = 0; i<lenght; i++) {
			try {
				if(romeToDez(roem.charAt(i))>=(romeToDez(roem.charAt(i+1)))) {
					sum = sum+romeToDez(roem.charAt(i));
				} else {
					sum = sum+(romeToDez(roem.charAt(i+1))-romeToDez(roem.charAt(i)));
					i++;
				}
			} catch(Exception e) {
				sum = sum+romeToDez(roem.charAt(i));
				
			}
		}
		return sum;
	}
}

