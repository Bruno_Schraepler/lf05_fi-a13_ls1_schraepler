import java.util.Scanner;

public class Schleifen1_Aufgabe8_Quadrat {

	public static void main(String[] args) {
		
		//Variable definieren
		short seitenlaenge;
		
		//Scannerobjekt erschaffen
		Scanner scanner = new Scanner(System.in);
		
		//Seitenlaenge abfragen
		System.out.println("Bitte geben Sie die Seitenlaenge ein: ");
		seitenlaenge = scanner.nextShort();
		
		drawSquare(seitenlaenge);
		
		scanner.close();
		
	}

	public static void drawSquare(short seitenlaenge) {
		//Methode um Quadrat auszugeben
		char randZeichen = '*';
		for(int i = 1; i <= seitenlaenge; i++) {
			System.out.println();
			for(int j = 1; j <= seitenlaenge; j++) {
				if((i==1) || (i==seitenlaenge)) {
					System.out.print(randZeichen+" ");			//Space hinzufügen um Ausgabe in der Konsole echt quadratisch auszugeben
				} else {
					if((j == 1) || (j == seitenlaenge)) {
						System.out.print(randZeichen+" ");		
					} else {
						System.out.print("  ");
					}
				}
				
			}
		}
		
	}

}

