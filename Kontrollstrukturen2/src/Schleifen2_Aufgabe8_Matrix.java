import java.util.Scanner;


public class Schleifen2_Aufgabe8_Matrix {

	public static void main(String[] args) {
		matrix_standard();
		
		Scanner scanner = new Scanner(System.in);
		
		//Eingabe vom User zur Pruefzahl
		System.out.println("\n\nGeben sie die gew�nschte Pruefzahl (2-9): ");
		int pruefzahl = scanner.nextInt();
		
		matrix_modified(pruefzahl);

	}
	
	public static void matrix_standard() {
		for(int i = 0; i<100; i++) {
			if(i<10) {
				System.out.printf(" %d ", i);
			} else {
				if(i%10 == 0) {
					System.out.println();
				}
				System.out.printf("%d ", i);
			}
			
		}
	}
	
	public static int quersumme(int zahl) {
		//Methode gibt die Quersumme einer mitgegebenen Zahl wieder (f�r Zahlen <100)
		return ((zahl%10) + (zahl/10));
	}
	
	public static boolean isPartOf(int testzahl, int pruefzahl) {
		//Methode pr�ft ob die gegebene Pr�fzahl in der gegebenen Testzahl vorhanden ist (f�r Zahlen <100)
		boolean partOf;
		if(testzahl%10 == pruefzahl || testzahl/10 == pruefzahl) {
			partOf = true;
		} else {
			partOf = false;
		}
		return partOf;
	}
	
	public static void matrix_modified(int pruefzahl) {
		System.out.print(" 0 "); //Die Zahl 0 ist von keiner Bedingung erfasst und kann daher au�erhalb der for-Schleife stehen
		for(int i = 1; i<100; i++) {
			if(i<10) {
				//Abfrage ob einer der Bedinungen erf�llt ist, um die Zahl gegen einen Stern zu ersetzten
				if((i%pruefzahl == 0) || (quersumme(i) == pruefzahl) || (isPartOf(i, pruefzahl)))  {
					System.out.print(" * ");
				} else {
					System.out.printf(" %d ", i);
				}
			} else {
				if(i%10 == 0) {
					System.out.println(); //Zeilenumbruch um Matrixformatierungs zu erhalten
				}
				//Abfrage ob einer der Bedinungen erf�llt ist, um die Zahl gegen einen Stern zu ersetzten
				if((i%pruefzahl == 0) || (quersumme(i) == pruefzahl) || (isPartOf(i, pruefzahl)))  {
					System.out.print(" * ");
				} else {
					System.out.printf("%d ", i);
				}
				
			}
			
		}
	}

}
