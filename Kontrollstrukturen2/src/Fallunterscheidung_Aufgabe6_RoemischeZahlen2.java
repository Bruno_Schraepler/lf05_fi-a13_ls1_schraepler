import java.util.Arrays;
import java.util.Scanner;

public class Fallunterscheidung_Aufgabe6_RoemischeZahlen2 {

	public static void main(String[] args) {
		//Scannerobjekt zur Abfrage der R�mischen Zahl als String erschaffen
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bitte geben sie die R�mische Zahl in Gro�buchstaben ein: ");
		String roem = scanner.next();
		
		//n�tige Variablen definieren
		int length = roem.length();
		int sum = 0;
		
		//r�mische Zahl in Dezimalzahl umwandeln
		System.out.println(umwandeln(roem, length, sum));
		
	}
	
	//Methode zum Umwandeln einzelner r�mischer Zeichen in Dezimalzahlen
	public static int romeToDez(char roem) {
		switch(roem) {
		case 'I':
			return 1;
		case 'V':
			return 5;
		case 'X':
			return 10;
		case 'L':
			return 50;
		case 'C':
			return 100;
		case 'D':
			return 500;
		case 'M':
			return 1000;
		default:
			System.out.print("Invalid Character: "+roem);
			return 0;
		}
	}
	
	//check-Methoden �berpr�fen auf ung�ltige Eingaben
	
	public static boolean checkFor_3gleicheZeichen(String roem, int length) {
		//Methode �berpr�ft, ob mehr als 3 gleiche Zeichen hintereinander stehen, wenn "ja"(Fall: Eingabe ung�ltig) wird true ausgegeben, 
		//wenn "nein"(Fall: Eingabe g�ltig) false
		
		boolean checkVariable = false;
		for(int i = 0; i<length; i++) {
			try {
				if(roem.charAt(i+1) == roem.charAt(i)) {
					if(roem.charAt(i+2) == roem.charAt(i)) {
						if(roem.charAt(i+3) == roem.charAt(i)) {
							checkVariable = true;
							break;
						}
					}
				}
			} catch (Exception e) {
				checkVariable = false;
			}
		}
		return checkVariable;
	}
	
	public static boolean checkFor_ungueltigeZeichenwiederholung(String roem, int length) {
		//Methode �berpr�ft ob eine ung�ltige Zeichenwiederholung eingegeben wurde (bspw. VV oder LL)
		
		boolean checkVariable = false;
		for(int i = 0; i < length; i++) {
			if(Arrays.asList('V', 'L', 'D').contains(roem.charAt(i))) {
				try {
					if(roem.charAt(i) == roem.charAt(i+1)) {
						checkVariable = true;
						break;
					}
				} catch(Exception e) {
					checkVariable = false;
				}
				
			}
		}
		return checkVariable;
	}
	
	public static boolean checkFor_ungueltigeReihenfolge(String roem, int length) {
		//Methode �berpr�ft auf ung�ltige Zahlenteihenfolgen (kleinere Zahlen folgen auf gr��ere Zahlen)
		boolean checkVariable = false;
		for(int i = 0; i < length; i++) {
			if(i == length-2) {
				break;
			} else {
				if(romeToDez(roem.charAt(i)) < romeToDez(roem.charAt(i+2))) {
					checkVariable = true;
					break;
				}
			}
		}
		return checkVariable;
	}
	
	public static int umwandeln(String roem, int length, int sum) {
		if(checkFor_3gleicheZeichen(roem, length)) {
			System.out.println("Ung�ltige Eingabe! Es d�rfen maximal 3 gleiche Zeichen hintereinander stehen");
			System.exit(0);
		} else if(checkFor_ungueltigeZeichenwiederholung(roem, length)) {
			System.out.println("Ung�ltige Eingabe! Die Zeichen V, L und D d�rfen niemals doppelt stehen");
			System.exit(0);
		} else if(checkFor_ungueltigeReihenfolge(roem, length)) {
			System.out.println("Ung�ltige Eingabe! Reihenfolge ist nicht korrekt: kleinere Zahlen folgen auf gr��ere Zahlen");
			System.exit(0);
		} else {
			boolean lastLetter = false;
			for(int i = 0; i < length; i++) {
				if(i == length-1) {
					lastLetter = true;
					sum = sum+romeToDez(roem.charAt(i));
					break;
				} else {
					if(romeToDez(roem.charAt(i)) >= romeToDez(roem.charAt(i+1))){
						sum = sum+romeToDez(roem.charAt(i));
					} else {
						if(Arrays.asList('I', 'X', 'C').contains(roem.charAt(i))) {
							if(roem.charAt(i) == 'I') {
								if(Arrays.asList('V', 'X').contains(roem.charAt(i+1))) {
									sum = sum+(romeToDez(roem.charAt(i+1))-romeToDez(roem.charAt(i)));
									i++;
								} else {
									System.out.println("Ung�ltige Eingabe! I darf nur von V oder X abgezogen werden");
									System.exit(0);
								}
							} else if(roem.charAt(i) == 'X') {
								if(Arrays.asList('L', 'C').contains(roem.charAt(i+1))){
									sum = sum+(romeToDez(roem.charAt(i+1)) - romeToDez(roem.charAt(i)));
									i++;
								} else {
									System.out.println("Ung�ltige Eingabe! X darf nur von L und C abgeozgen werden");
									System.exit(0);
								}
							} else if(roem.charAt(i) == 'C') {
								if(Arrays.asList('D', 'M').contains(roem.charAt(i+1))) {
									sum = sum+(romeToDez(roem.charAt(i+1))-romeToDez(roem.charAt(i)));
									i++;
								} else {
									System.out.println("Ung�ltige Eingabe! C darf nur von D oder M abgezogen werden");
									System.exit(0);
								}
							}
						} else {
							System.out.println("Ung�ltige Eingabe! Subtraktion ist ausschlie�lich erlaubt mit I,X und C");
							System.exit(0);
						}
					}
				}
			}
		}
		return sum;
	}
}